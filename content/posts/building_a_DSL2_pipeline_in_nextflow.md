---
date: 2019-12-17
title: "Building a DSL2 pipeline in Nextflow"
draft: false
---

## Introduction
This post will describe migration or building analytical pipelines using version 2 of the DSL (domain specific language) that the [Nextflow](https://www.nextflow.io/) workflow manager implements

### Why Nextflow?

I have been using [Nextflow](https://www.nextflow.io/) for several years now because it has enabled writing analytical pipelines that are

 * Reproducible
 * Shareable
 * Infrastructure agnostic
 * Scalable

There are many other workflow managers, but for the way my mind works I have found that the declarative, process-oriented, top to bottom approach of Nextflow more intuitive than the make-like, output-oriented, bottom to top approach of Snakemake for example. Your experience may vary and I would encourage you to look at a few and choose the one that best matches your requirements and the 'way you think. There are a few tables that compare different workflow managers ([here](https://vatlab.github.io/blog/post/comparison/) and [here](https://www.nature.com/articles/nbt.3820.epdf?author_access_token=NR5Sw5j7DhVJhsvhZX3O8NRgN0jAjWel9jnR3ZoTv0O-x-SaU0X4rrftBAum396PO2HW7nu_CY6WUD5BczKSljFiztaC_YS4qiOe1WqLsYegnGM2iRixif0sggcgrW7Y) for example) which may help guide your decision

### Why DSL2?

When I first started in bioinformatics I learnt Perl and picked up some bad habits (not necessarily Perl's fault though I don't think the language helped). I would write very long procedural scripts that did the job but were hard to understand, debug, test and re-factor. Equally badly in my opinion were some occasional very powerful but indecipherable Perl one-liners! I'm sure anybody that has been in bioinformatics for a while has seen one or two of those.   
I moved onto Ruby, Java and more latterly Python and learnt the power of functional and object-oriented programming. The ability to have short 'main' scripts with imported and re-usable functions made for much more readable, testable and portable code.

Therefore when I came to Nextflow, there was much to love but I had a couple of gripes.

1. The DSL used to describe pipelines was in Groovy whereas I would have preferred Python (my go-to language). In this instance it was just a case of 'suck it up' and learn the basics of a new language. To make powerful multi-step Nextflow pipelines does not require you to be an advanced Groovy programmer and anybody with a small amount of programming experience will be able to pick up the syntactical differences compared to their language of choice.
2. Although Nextflow and Groovy supports functions there was no ability to import Nextflow functions. (It should be noted that automatic importing of 'helper' functions written in Groovy from  scripts within a lib directory is possible).  
The consequence of this is that there are many excellent Nextflow pipelines available publicly but they can be 100s or even 1000s of lines long. The flow of these processes can be hard to pick out from the dense code. In addition reuse of processes between pipelines was not possible except through copy and paste. 

The second point was widely recognised by users as a short-coming and was the most requested feature in the community. Thankfully the main developer Paolo Di Tommaso listened and in May 2019 [announced](https://www.nextflow.io/blog/2019/one-more-step-towards-modules.html) DLS2. Since May this feature (still only in Edge/non-stable releases) has undergone much development and is nearing release and being feature-complete for building usable pipelines. In this post I want to describe how I have been developing pipelines in DSL2. I have learnt a lot from Paolo and the Nextflow community and want to acknowledge their invaluable help, much of which is through the active [gitter channel](https://gitter.im/nextflow-io/nextflow).

## Objectives

To build a pipeline where the main script is as concise as possible and the verbosity and complexity is contained within functions that are imported. As far as possible all paths should 'just work' and not require the user to edit these to match their own environment.

## Methodology

1. Bundle together processes and workflows into small packages that perform one or more sub-tasks but work together to perform just one unified task.  
In a similar way to good functional programming if a library does just 'one job' then it is easier to compose several of these into large supersets of functionality that perform a bigger function. A workflow (as defined by Nextflow) is a set of processes that take some inputs through a series of steps to produce a defined output. By bundling together processes that will together perform a single function through a workflow, these workflows can then be combined to make a larger pipeline.
2. Use git subtree or a derivative to enable easy reuse without having to worry about import paths.  
One issue that I only recently came to an acceptable solution for was how to include modules in a way that did not require modules to be in a specific hard-coded relative or absolute path outside of the pipeline. Based on the suggestion of Paolo, I investigated using [git subtree](https://www.atlassian.com/git/tutorials/git-subtree) or in my case [git subrepo](https://github.com/ingydotnet/git-subrepo) which wraps git subtree with some nice functionality. This allows other git repositories to be included in another parent repository as actual files rather than links as implemented by [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules), which is no longer recommended as best practice.
3. Import param_parsing, messaging and utility functions to keep the main script lightweight


This methodology is depicted in the image below
![DSL2 setup](/images/generic_dsl2_setup.png)

 * Each subdirectory in the modules directory contains a bundle of processes related to one main task linked together by a workflow to perform that single task. Each one is its own git repository
 * Each pipeline imports those functions using `git subrepo` into the `lib/modules` sub directory. If more than one workflow needs to be imported then they are given their own subdirectory in modules
 * static resources such as flat file databases are also maintained in their own sub-directory (in the diagram above for example, `data_sources/database_X` is its own git repository.) These are also incorporated into the main pipeline using `git subrepo`.

## Implementation
 For the rest of this post I will work through converting an existing pipeline and use this as an example of how similar migrations can be made or the same principles used to build a pipeline from scratch.

 The pipeline I want to implement is a simple one that calls [MLST types](https://en.wikipedia.org/wiki/Multilocus_sequence_typing) from bacterial genomes. I already have a [DSL1 version](https://gitlab.com/cgps/ghru/pipelines/mlst) of the pipeline that imports some utility 'helper functions' but I want to make it DSL2 in order to make the main script even cleaner and to incorporate a read polishing step that trims and corrects reads before typing. The latter step is something that I want to incorporate prior to many other downstream tasks so makes perfect sense to be an imported using the DLS2 functionality.

 1. **Add helper functions**  
    First off basic helper functions are added to a `lib` directory and imported into the `main.nf` workflow file.  
    See [here](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/tree/basic_helper_functions) for the project code at this stage.  
    ` `  
    There are functions to
    * [define default params](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/basic_helper_functions/lib/params_parser.nf#L4)
    * [print help or version](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/basic_helper_functions/lib/params_utilities.nf#L3)
    * [check user params merged with defaults](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/basic_helper_functions/lib/params_parser.nf#L23) and return a `final_params` map. 

2. **Include read polishing sub workflow using git subrepo**  
    There are already modules for [read polishing](https://gitlab.com/cgps/ghru/pipelines/dsl2/modules/read_polishing). Using the `git subrepo` [command](https://github.com/ingydotnet/git-subrepo) these modules can be cloned into the `lib/modules` directory
    ```
    git subrepo clone git@gitlab.com:cgps/ghru/pipelines/dsl2/modules/read_polishing.git lib/modules/read_polishing
    ```
     See [here](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/tree/read_polishing_include) for the project code at this stage.  
 ` `  
    To utilise this functionality several requirements need to be fulfilled  
    1. The read_polishing workflow must be [included](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/read_polishing_include/main.nf#L31). Because this is relative to the main workflow this path can be hardcoded. The `final_params` derived from the defaults and those passed to the main.nf script via `nextflow run` (see previous step) will be passed to this workflow in the include statement using `params(final_params)`.

        ```groovy
        include './lib/modules/read_polishing/workflows' params(params_for_read_polishing)
        ```
    2. Before this can be done the parameters for the main workflow may need some parsing so they match the requirements of the read_polishing sub-workflow. Since it possible that a parameter with the same name can be used twice in the included sub-workflows but with different values, my preference is to prefix the parameters with the sub-workflow name and then remove the prefix when passing the params onto the sub-workflow. I do this using a utility function called [rename_params_keys](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/read_polishing_include/lib/params_utilities.nf#L43) and then [call this function](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/read_polishing_include/main.nf#L26) before including the sub-workflow. In this case `read_polishing_adapter_file` and `read_polishing_depth_cutoff` are renamed as `adapter_file` and `depth_cutoff` respectively.

        ```groovy
        // include read polishing functionality
        read_polising_params_renames = [
        "read_polishing_adapter_file" : "adapter_file",
        "read_polishing_depth_cutoff" : "depth_cutoff"
        ]
        params_for_read_polishing = rename_params_keys(final_params, read_polising_params_renames)
        ```
3. **Call the read polishing sub-workflow from a master workflow**  
    To test the main workflow is working and calling the first of its sub-workflows a workflow must be defined within the main.nf file. If a workflow does not have a name `workflow <NAME> {...}` then it is assumed that it it is the main workflow and will be the one executed by `nextflow run main.nf ...`  
    See [here](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/tree/initial_workflow_definition) for the project code at this stage.  
    ` `  
    Within this workfow, a simple input channel derived form a glob path matching reads is defined.
    ```groovy
    Channel
        .fromFilePairs( final_params.reads_path )
        .ifEmpty { error "Cannot find any reads matching: ${final_params.reads_path}" }
        .set { reads }
    ```
    However it is the next line where all the DLS2 magic happens. The line is 
    ```groovy
    polished_reads = polish_reads(reads)
    ```
    Here the reads channel is passed as an argument to `polish_reads`. This is a workflow defined in the included fom the `read_polishing/workflows` [module](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/read_polishing_include/lib/modules/read_polishing/workflows.nf)

    The code is 
    ```groovy
    include './processes'
    workflow polish_reads {
    take: reads

    main:
        // get read lengths
        read_lengths = determine_min_read_length(reads)
        reads_and_lengths = reads.join(read_lengths)

        // trim reads
        trimmed_reads = trim_reads(reads_and_lengths, params.adapter_file)

        ......
    emit:
        downsampled_reads

    }
    ```
    This consists of 3 blocks  
    1. **take:**  
    This specifies that the workflow expects one argument to be passed to it. In this case a channel of reads.
    2. **main:**  
    This section includes the processes that are called. In this case a process called `determine_min_read_length` is called passing in the `reads` channel as an input. The output from this process is captured in `read_lengths` and in the next line this is joined to the original reads channel as `reads_and_lengths`. This combined channel is passed as an input to the `trim_reads` process and the output from this process captured as `trimmed_reads`. It should be mentioned at this stage that rather than using the assignment `=` to capture outputs the output from a process can be specified as `<PROCESS NAME>.out`. So rather than `read_lengths` been used as the named output from the `determine_min_read_length` process, the output could also be passed as `determine_min_read_length.out`. 
    3. **emit:**  
    This section specifies which process outputs are accessible to downstream sub-workflows.

    The processes themselves are defined by the [include line](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/initial_workflow_definition/lib/modules/read_polishing/workflows.nf#L1) before the workflow. Taking a look at the definitions of these processes in [processes.nf](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/initial_workflow_definition/lib/modules/read_polishing/processes.nf) you will see that they are just like regular Nextflow processes except that the input block has no `from` statements and the output block has no `into` statements. Instead the expectation is that processes are called and inputs specified as arguments and the outputs are either captured using assignment or using the `<PROCESS NAME>.out` notation within the workflow definitions. Another thing to note is that the old `set` syntax is replaced with `tuple` which more clearly defines its meaning.
4. **Test the sub-workflow and specificy dependencies**  
    Having provided some test files in `test_input` it is now possible to test the sub-workflow once dependencies have been specified.  
    See [here](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/tree/basic_helper_functions) for the project code at this stage.  
    ` `  
    The dependencies can be specified by describing the container for each process within the [nextflow.config](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/first_test_run/nextflow.config) file.  
    Two pairs of fastqs have been provided as input files within the `test_input` directory and the pipeline can now be run and the read_polishing sub-workflow carried out
    ```
    NXF_VER=19.11.0-edge nextflow run main.nf --input_dir $PWD/test_input --fastq_pattern '*{R,_}{1,2}.fastq.gz' --output_dir $PWD/test_output -resume
    ```
    It is important to set the NXF_VER environmental variable to the latest edge version since DSL2 is not yet in stable releases.  
    The warnings you will see are incorrect
    ![Incorrect warnings](/images/dsl2_run_warnings.png)
    I presume these will be fixed before DSL2 becomes stable
5. **Incorporate DSL1 MLST calling workflow**  
    The DSL1 MLST pipeline can be found [here](https://gitlab.com/cgps/ghru/pipelines/mlst). The first thing to do is to extract the workflow in the main.nf file and its processes into two files, one for the workflow and one for the processes. The following are changed in the processes.nf file
    *  remove the `from` and `into` statements from the input and outputs blocks.
    * change `set` to `tuple`.
    * change `file` to `path`.
    * where a process has more than one output name them using emit keywords.  
  See [processes.nf](https://gitlab.com/cgps/ghru/pipelines/dsl2/modules/mlst/blob/master/processes.nf)  
    ` `  
    In the workflows file the following are required
    * importation of the processes, passing in the params since the scope of variables is local to each file.
    * specification that the workflow takes a reads channel as inputs using the `take` keyword.
    * within the `main` block call the two processes, the first using the reads channel and the second using the named output `mlst_reports.simple_reports` from the first process which are collected so that the reports are processed together in the summary step.  

    See [workflows.nf](https://gitlab.com/cgps/ghru/pipelines/dsl2/modules/mlst/blob/master/workflows.nf) and also below
    ```groovy
    include './processes' params(params)
    workflow run_mlst{
        take: reads

        main:
        mlst_reports = run_ariba(reads, params.species_db)
        collected_reports = mlst_reports.simple_reports.collect( sort: {a, b -> a[0].getBaseName() <=> b[0].getBaseName()} )
        combine_mlst_reports(collected_reports)
    }
    ```
    To make these files available in the master pipeline the code is cloned using git subrepo
    ```
    git subrepo clone git@gitlab.com:cgps/ghru/pipelines/dsl2/modules/mlst.git lib/modules/mlst
    ```

    This workflow is then included in main.nf file of the master pipeline having renamed params as appropriate
    ```groovy
    // include mlst functionality
    mlst_params_renames = [
    "mlst_species_db" : "species_db"
    ]
    params_for_mlst = rename_params_keys(final_params, mlst_params_renames)
    include './lib/modules/mlst/workflows' params(params_for_mlst)
    ```
    This included functionality is then called from the main workflow
    ```groovy
    run_ariba(polished_reads)
    ```
6. **Including data sources**  
    In order to call the MLST type with ariba, pre-prepared databases are required. These are included from another git repo using a subrepo command
    ```
    git subrepo clone git@gitlab.com:cgps/ghru/pipelines/data_sources/ariba_mlst_databases.git mlst_databases
    ```
    When specifying these databases the species parameter is converted to a path that passes a database directory within the main repo to the `run_mlst` workflow using the syntax `file("${workflow.projectDir}/mlst_databases/${species_db_name}/ref_db")`  
    See [params_parser.nf](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/blob/run_complete_workflow/lib/params_parser.nf#L51) and below:
    ```groovy
    check_mandatory_parameter(params, 'mlst_species')
    // check species is valid
    check_parameter_value('species', params.mlst_species, ['Acinetobacter baumannii', 'Enterobacter cloacae', 'Enterococcus faecalis', 'Enterococcus faecium', 'Escherichia coli', 'Klebsiella pneumoniae', 'Neisseria spp.', 'Pseudomonas aeruginosa', 'Salmonella enterica', 'Staphylococcus aureus', 'Staphylococcus haemolyticus', 'Streptococcus pneumoniae', 'Vibrio cholerae'])
    // make mlst db name from species
    species_db_name = params.mlst_species.replaceFirst(/ /, '_').toLowerCase()
    final_params.mlst_species_db = file("${workflow.projectDir}/mlst_databases/${species_db_name}/ref_db")
    ```
7. **Including dependencies**  
    Finally dependencies are included in the nextflow.config these take the format of 
    ```
    process {
        ...
        withName:'<WORKFLOW NAME>:<PROCESS NAME>' {
            container = '<DOCKER OR SINGULARITY CONTAINER>'
        }
    }
    ```
    So for the two processes of the run_mlst workflow this would be 
    ```
    withName:'run_mlst:run_ariba' {
        container = 'bioinformant/ghru-mlst:1.0'
    }
    withName:'run_mlst:combine_mlst_reports' {
        container = 'bioinformant/ghru-mlst:1.0'
    }
    ```
    The final working version of the code can be found [here](https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/mlst/tree/run_complete_workflow)
8. **Running the final pipeline**  
    Now a species from the allowed list needs to be specified as a param. Therefore for the two *Staphylococcus aureus* test samples the command would be  
    ```
    NXF_VER=19.11.0-edge nextflow run main.nf --input_dir $PWD/test_input --fastq_pattern '*{R,_}{1,2}.fastq.gz' --output_dir $PWD/test_output --mlst_species 'Staphylococcus aureus' -resume
    ```
    Again when running the pipeline ignore the warnings about processes not matching a config selector.

## Conclusion
By using the modular functionality of DSL2 it is possible to achieve
1. A very readable main.nf pipeline
2. Import modules into multiple pipelines
3. Using `git subrepo` it is possible to keep all the modules and data sources versioned but contained within a single pipeline repo.

The final `main.nf` is just 50 lines long compared to 137 lines in teh original file and has double the functionality incorporating a read polishing workflow prior to calling MLST. See below.

Please add comments or suggests using the Disqus feature at the end of this post.
```groovy
// DSL 2
nextflow.preview.dsl=2
version = '1.0'

// include helper functions
include './lib/params_parser'
include './lib/params_utilities'

// setup params
default_params = default_params()
merged_params = default_params + params

// help and version messages
help_or_version(merged_params, version)

final_params = check_params(merged_params, version)

// include read polishing functionality
read_polising_params_renames = [
  "read_polishing_adapter_file" : "adapter_file",
  "read_polishing_depth_cutoff" : "depth_cutoff"
]
params_for_read_polishing = rename_params_keys(final_params, read_polising_params_renames)
include './lib/modules/read_polishing/workflows' params(params_for_read_polishing)

// include mlst functionality
mlst_params_renames = [
  "mlst_species_db" : "species_db"
]
params_for_mlst = rename_params_keys(final_params, mlst_params_renames)
include './lib/modules/mlst/workflows' params(params_for_mlst)

workflow {
  //Setup input Channel from Read path
  Channel
      .fromFilePairs( final_params.reads_path, checkIfExists: true )
      .set { reads }

  polished_reads = polish_reads(reads)
  run_mlst(polished_reads)

}
```



