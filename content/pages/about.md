---
title: About me
subtitle: A little bit about me
comments: false
---

My name is Anthony Underwood

- I currently work for the [Centre for Genomic Pathogen Surveillance](http://www.pathogensurveillance.net/)
- My role is as Bioinformatics Implementation Manager for the [NIHR Global Health Research Unit for Genomic Surveillance of Antimicrobial Resistance](http://ghru.pathogensurveillance.net/).
- The purpose of this project is to
  - Build capacity in WGS for AMR surveillance from the ground up in both laboratory techniques and bioiformatics
  - To produce data for national action
  - To produce data that is open to the Public Health community
  - To perform structured surveillance of pathogens for AMR

### My work passions
 - Python
 - Reproducible code for bioinformatics
